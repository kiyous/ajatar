package net.daemonyum.ajatar.annotation.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation à utiliser dans une DaemonyumServlet.
 * Doit �tre utiliser sur un element de type String[].
 * Permet de créer ou modifier le contenu d'un cookie de la même façon que addCookie sur un objet HttpServletResponse.
 * @author Kiyous
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SaveCookie {	

}
