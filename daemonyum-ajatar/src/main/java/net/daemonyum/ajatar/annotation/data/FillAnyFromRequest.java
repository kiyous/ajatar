package net.daemonyum.ajatar.annotation.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation � utiliser dans une DaemonyumServlet.
 * Doit �tre utiliser sur un element de type String[].
 * Permet de r�cup�rer le contenu de param�tres du m�me nom de la m�me fa�on que getParameterValues sur un objet HttpServletRequest.
 * @author Kiyous
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FillAnyFromRequest {
	
	/** Nom de la variable en request */
	String value();

}
