package net.daemonyum.ajatar.annotation.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation � utiliser dans une DaemonyumServlet.
 * Doit être utiliser sur un element de type Object.
 * Permet de mettre en session une variable de la même façon que getSession().setAttribute sur un objet HttpServletRequest.
 * @author Kiyous
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SendToSession {
	
	/** Nom de la variable en session */
	String value();
	
	boolean condition() default true;

}
