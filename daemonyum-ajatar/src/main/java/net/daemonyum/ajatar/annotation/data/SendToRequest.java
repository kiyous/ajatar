package net.daemonyum.ajatar.annotation.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation � utiliser dans une DaemonyumServlet.
 * Doit �tre utiliser sur un element de type Object.
 * Permet d'envoyer une variable vers la vue de la servlet de la m�me fa�on que setAttribute sur un objet HttpServletResponse.
 * @author Kiyous
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SendToRequest {
	
	/** Nom de la variable en request */
	String value();
	
	boolean condition() default true;

}
