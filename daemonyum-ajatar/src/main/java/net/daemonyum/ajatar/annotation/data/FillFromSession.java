package net.daemonyum.ajatar.annotation.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation à utiliser dans une DaemonyumServlet.
 * Doit être utiliser sur un element de type Object.
 * Permet de récupérer le contenu d'un attribut de session de la même façon que getSession().getAttribute sur un objet HttpServletRequest.
 * @author Kiyous
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FillFromSession {

	net.daemonyum.ajatar.parameter.Clean clean() default net.daemonyum.ajatar.parameter.Clean.FALSE;
	/** Nom de la variable en session */
	String value();

}
