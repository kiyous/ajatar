package net.daemonyum.ajatar.annotation.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation à utiliser dans une DaemonyumServlet.
 * Doit �tre utiliser sur un element de type String[].
 * Permet de récupérer le contenu d'un cookie de la même façon que getCookies sur un objet HttpServletRequest.
 * @author Kiyous
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface RecoverCookie {
	
	/** Nom du cookie */
	String value();

}
