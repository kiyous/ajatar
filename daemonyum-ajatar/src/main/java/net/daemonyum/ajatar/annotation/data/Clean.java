package net.daemonyum.ajatar.annotation.data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import net.daemonyum.ajatar.parameter.Scope;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Clean {
	
	Scope scope();
	String value();

}
